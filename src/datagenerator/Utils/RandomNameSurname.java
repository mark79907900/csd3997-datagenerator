/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.Utils;

import datagenerator.Files.Files;
import datagenerator.Types.NameSurname;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class RandomNameSurname {
    private static int min = 0;
    
    public static String getRandomName(){
        return getNameSurname().getName();
    }
    
    public static String getRandomSurname(){
        return getNameSurname().getSurname();
    }
    
    /**
     * get a random NameSurname object
     * @return
     */
    private static NameSurname getNameSurname(){
        NameSurname nameSurname = null;
        try{
            int max = Files.getNameSurnameList().size()-1;
            nameSurname = Files.getNameSurnameList().get(RandomNumber.getRandomInt(min, max));
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
        return nameSurname;
    }
}
