/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Static class which deals with datetime
 * @author Mark
 */
public class TimeConvertor {
    private static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    /**
     * Convert a timestamp to readable date format
     * @param timeStamp
     * @return 
     */
    public static String toString(long timeStamp){
        return dateFormat.format(new Date(timeStamp/1000));
    }
    
}
