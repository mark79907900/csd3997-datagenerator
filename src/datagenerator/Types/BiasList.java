/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.Types;

import datagenerator.Utils.ErrorHandling;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class BiasList {
    //the long is the bias id, this way it is much faster to get the requested bias
    private Map<Long, Bias> biasMap;
    
    public BiasList() {
        this.biasMap = new HashMap<Long, Bias>();
    }
    
    /**
     * add a new Bias
     * @param bias
     */
    public void add(Bias bias){
        long biasId = this.biasMap.size()+1;
        
        //make sure it is a unquie key
        while(this.biasMap.containsKey(biasId)){
            biasId++;
        }
        
        bias.setBiasId(biasId);
        this.biasMap.put(biasId, bias);
    }
    
    /**
     * Update a bias in the list
     * @param bias 
     */
    public void update(Bias bias){
        this.biasMap.put(bias.getBiasId(), bias);
    }
    
    /**
     * remove a bias from the list
     * @param biasId 
     */
    public void remove(long biasId){
        this.biasMap.remove(biasId);
    }
    
    /**
     * check if the list is empty
     * @return
     */
    public boolean isEmpty(){
        return this.biasMap.isEmpty();
    }
    
    /**
     * get an object array for the given route with all the BiasIDs
     * @return
     */
    public Object[] getBiasIdObjectArray(){
        ArrayList<Long> biasIdList = null;
        try{
            biasIdList = new ArrayList<Long>();
            for(Entry<Long, Bias> bias : biasMap.entrySet()){
                biasIdList.add(bias.getKey());
            }
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
        return biasIdList.toArray();
    }
    
    /**
     * Get a bias object from its key
     * @param biasId
     * @return 
     */
    public Bias getBais(long biasId){
        Bias bias = null;
        try{
            bias = biasMap.get(biasId);
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return bias;
    }
    
    /**
     * get BiasMap
     * @return 
     */
    public Map<Long, Bias> getBiasMap() {
        return biasMap;
    }
}
