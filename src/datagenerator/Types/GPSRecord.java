/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.Types;

import datagenerator.Utils.Config;
import datagenerator.Utils.ErrorHandling;
import datagenerator.Utils.TimeConvertor;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class GPSRecord {
    private long timeStamp;
    private int lineID;
    private int direction;
    private String journeyPatternID;
    private String timeFrame;
    private Integer vehicleJourneyID;
    private String operator;
    private int congestion;
    private double lon;
    private double lat;
    private int delay;
    private int blockID;
    private int vehicleID;
    private Integer stopID;
    private int atStop;
    
    /**
     * create a new GPSRow object from the line.
     * This line is read from the CSV file.
     * @param lineValue
     */
    public GPSRecord(String lineValue) {
        try{
            String[] parts = lineValue.split(",");
            
            //throw exception if the gps point passed does not have the expected number of columns
            int colCount = Config.getPropertyInt("GPSDataSet_NumberOfColumns");
            if(parts.length != colCount){
                ErrorHandling.throwException(
                        String.format("This GPS point does not have %d columns, it has %d", colCount, parts.length)
                );
            }
            
            timeStamp = Long.parseLong(parts[0]);
            if(parts[1].isEmpty()){
                lineID = -1;
            }else{
                lineID = Integer.parseInt(parts[1]);
            }
            direction = Integer.parseInt(parts[2]);
            if(parts[3].equals("null")){
                journeyPatternID = null;
            }else{
                journeyPatternID = parts[3];
            }
            timeFrame = parts[4];
            vehicleJourneyID = Integer.parseInt(parts[5]);
            operator = parts[6];
            congestion = Integer.parseInt(parts[7]);
            lon = Double.parseDouble(parts[8]);
            lat = Double.parseDouble(parts[9]);
            delay = Integer.parseInt(parts[10]);
            blockID = Integer.parseInt(parts[11]);
            vehicleID = Integer.parseInt(parts[12]);
            if(parts[13].equals("null")){
                stopID = -1;
            }else{
                stopID = Integer.parseInt(parts[13]);
            }
            atStop = Integer.parseInt(parts[14]);
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    /**
     * get the line id of this GPS row object
     * @return
     */
    public int getLineID() {
        return lineID;
    }
    
    /**
     * get the stop id of this GPS row Object
     * @return
     */
    public Integer getStopID() {
        return stopID;
    }
    
    public long getTimeStamp() {
        return timeStamp;
    }
    
    public int getAtStop() {
        return atStop;
    }

    public double getLon() {
        return lon;
    }

    public double getLat() {
        return lat;
    }
    
    @Override
    public String toString(){
        return "" + timeStamp + "," +
                lineID + "," +
                direction + "," +
                journeyPatternID + "," +
                timeFrame + "," +
                vehicleJourneyID + "," +
                operator + "," +
                congestion + "," +
                lon + "," +
                lat + "," +
                delay + "," +
                blockID + "," +
                vehicleID + "," +
                stopID + "," +
                atStop + "\n";
    }
    
    public static String toStringTtiles(){
        return "TimeStamp,Line ID,Direction,Journey Pattern ID,Time Frame,"
                + "Vehicle Journey ID,Operator,Congestion,Lon,Lat,Delay,"
                + "Block ID,Vehicle ID,Stop ID,At Stop\n";
    }
    
    public String toStringFull(){
        return "" + timeStamp + "," +
                TimeConvertor.toString(timeStamp) + "," +
                lineID + "," +
                direction + "," +
                journeyPatternID + "," +
                timeFrame + "," +
                vehicleJourneyID + "," +
                operator + "," +
                congestion + "," +
                lon + "," +
                lat + "," +
                delay + "," +
                blockID + "," +
                vehicleID + "," +
                stopID + "," +
                atStop + "\n";
    }
    
    public static String toStringFullTitles(){
        return "TimeStamp,DateTime, Line ID,Direction,Journey Pattern ID,Time Frame,"
                + "Vehicle Journey ID,Operator,Congestion,Lon,Lat,Delay,"
                + "Block ID,Vehicle ID,Stop ID,At Stop\n";
    }
    
    public String toStringShort(){
        return "" + timeStamp + "," +
                TimeConvertor.toString(timeStamp) + "," +
                lineID + "," +
                lon + "," +
                lat + "," +
                stopID + "\n";
    }
    
    public static String toStringShortTitles(){
        return "TimeStamp,DateTime,Line ID,Lon,Lat,Stop ID\n";
    }
}
