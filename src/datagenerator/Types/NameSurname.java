/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datagenerator.Types;

import datagenerator.Utils.ErrorHandling;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class NameSurname {
    private String name;
    private String surname;

    public NameSurname(String lineValue) {
        try{
            String[] parts = lineValue.split(",");
            
            int colCount = 2;
            if(parts.length != colCount){
                ErrorHandling.throwException(
                        String.format("This name/surname does not have %d columns, it has %d", colCount, parts.length)
                );
            }
            
            this.name = parts[0].replace(" ", "");
            this.surname = parts[1].replace(" ", "");
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
}
