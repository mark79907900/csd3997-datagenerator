/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.Types;

/**
 *
 * @author Mark
 */
public class Bias {
    private long biasId;
    private int routeId;
    private int StopId;
    private int minGps;
    private int maxGps;
    private int minPassenger;
    private int maxPassenger;
    private long timeStamp;
    private boolean dumpGPSData;
    private boolean dumpPassengerData;
    
    public Bias(int routeId, int StopId, int minGps, int maxGps, int minPassenger, int maxPassenger, long timeStamp, boolean gpsCheck, boolean passengerCheck) {
        this.biasId = -1;
        this.routeId = routeId;
        this.StopId = StopId;
        this.minGps = minGps;
        this.maxGps = maxGps;
        this.minPassenger = minPassenger;
        this.maxPassenger = maxPassenger;
        this.timeStamp = timeStamp;
        this.dumpGPSData = gpsCheck;
        this.dumpPassengerData = passengerCheck;
    }
    
    public void setBiasId(long biasId) {
        this.biasId = biasId;
    }
    
    public int getRouteId() {
        return routeId;
    }
    
    public int getStopId() {
        return StopId;
    }
    
    public int getMinGps() {
        return minGps;
    }
    
    public int getMaxGps() {
        return maxGps;
    }
    
    public int getMinPassenger() {
        return minPassenger;
    }
    
    public int getMaxPassenger() {
        return maxPassenger;
    }
    
    public long getTimeStamp() {
        return timeStamp;
    }

    public long getBiasId() {
        return biasId;
    }
    
    public boolean isDumpGPSData() {
        return dumpGPSData;
    }

    public boolean isDumpPassengerData() {
        return dumpPassengerData;
    }
}
