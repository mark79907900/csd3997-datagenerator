/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.Types;

import datagenerator.Utils.Config;
import datagenerator.Utils.ErrorHandling;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Stop {
    private int stopId;
    private String stopName;
    private double lat;
    private double lon;
    
    public Stop(String lineValue) {
        try{
            String[] parts = lineValue.split(",");
            
            //throw exception if the gps point passed does not have the expected number of columns
            int colCount = Config.getPropertyInt("stopsDataSet_NumberOfColumns");
            if(parts.length != colCount){
                ErrorHandling.throwException(
                        String.format("This Stop point does not have %d columns, it has %d", colCount, parts.length)
                );
            }
            
            this.stopId = Integer.parseInt(parts[0]);
            this.stopName = parts[1];
            this.lat = Double.parseDouble(parts[2]);
            this.lon = Double.parseDouble(parts[3]);
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }

    public int getStopId() {
        return stopId;
    }

    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }
    
    @Override
    public String toString(){
        return "" + stopId + "," +
                stopName + "," +
                lat + "," +
                lon + "\n";
    }
    
    public static String toStingTitles(){
        return "Stop ID,Stop Name,Lat,Lon\n";
    }
    
    public String toStringShort(){
        return "" + stopId + "," +
                lat + "," +
                lon + "\n";
    }
    
    public static String toStingShortTitles(){
        return "Stop ID,Lat,Lon\n";
    }
}
