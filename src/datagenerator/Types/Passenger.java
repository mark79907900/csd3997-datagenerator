/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.Types;

import datagenerator.Files.Files;
import datagenerator.Utils.Config;
import datagenerator.Utils.ErrorHandling;
import datagenerator.Utils.RandomNameSurname;
import datagenerator.Utils.RandomNumber;
import datagenerator.Utils.TimeConvertor;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Passenger {
    
    private final long cardNumber;
    private long timeStamp;
    private final int routeId;
    private final int busStopId;
    private final String name;
    private final String surname;
    private final double lon;
    private final double lat;
    
    public Passenger(long cardNumber, GPSRecord row) {
        this.cardNumber = cardNumber;
        this.timeStamp = row.getTimeStamp();
        this.routeId = row.getLineID();
        this.busStopId = row.getStopID();
        this.name = RandomNameSurname.getRandomName();
        this.surname = RandomNameSurname.getRandomSurname();
        this.lon = row.getLon();
        this.lat = row.getLat();
    }
    
    public Passenger(Passenger passenger, GPSRecord row, long secondBusTripTimeWindowLimt) {
        this.cardNumber = passenger.getCardNumber();
        long randomTimeStampIncrement = RandomNumber.getRandomLong(0, secondBusTripTimeWindowLimt);
        randomTimeStampIncrement = randomTimeStampIncrement - (randomTimeStampIncrement % 1000000);
        this.timeStamp = passenger.getTimeStamp() + randomTimeStampIncrement;
        this.name = passenger.getName();
        this.surname = passenger.getSurname();
        
        this.routeId = row.getLineID();
        this.busStopId = row.getStopID();
        this.lon = row.getLon();
        this.lat = row.getLat();
    }
    
    /**
     * method used to generate the passengers with a second difference from the pervious
     * @param index
     */
    public void updateTimeStamp(int index){
        this.timeStamp = this.timeStamp + (index*1000000);
    }
    
    // <editor-fold desc=" Getters ">
    public long getCardNumber() {
        return cardNumber;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }
    
    
    @Override
    public String toString(){
        return "" + cardNumber + "," +
                timeStamp + "," +
                routeId + "," +
                busStopId + "," +
                name + "," +
                surname + "," +
                lon + "," +
                lat + "\n";
    }
    
    public static String toStringTitles(){
        return "Card Number,TimeStamp,Route ID,Stop ID,Name,Surname,"
                + "Lon,Lat\n";
    }
    
    public String toStringFull(){
        return "" + cardNumber + "," +
                timeStamp + "," +
                TimeConvertor.toString(timeStamp) + "," +
                routeId + "," +
                busStopId + "," +
                name + "," +
                surname + "," +
                lon + "," +
                lat + "\n";
    }
    
    public static String toStringFullTitles(){
        return "Card Number,TimeStamp,DateTime,Route ID,Stop ID,Name,Surname,"
                + "Lon,Lat\n";
    }
    
    public String toStringShort(){
        return "" + cardNumber + "," +
                timeStamp + "," +
                TimeConvertor.toString(timeStamp) + "," +
                routeId + "," +
                busStopId + "," +
                lon + "," +
                lat + "\n";
    }
    
    public static String toStringShortTitles(){
        return "Card Number,TimeStamp,DateTime,Route ID,Stop ID,"
                + "Lon,Lat\n";
    }
    // </editor-fold>
}
