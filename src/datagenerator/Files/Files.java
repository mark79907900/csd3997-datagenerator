/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.Files;

import datagenerator.Types.Bias;
import datagenerator.Types.BiasList;
import datagenerator.Types.GPSRoute;
import datagenerator.Types.GPSRoutes;
import datagenerator.Types.GPSRecord;
import datagenerator.Types.NameSurname;
import datagenerator.Types.Passenger;
import datagenerator.Types.Stop;
import datagenerator.Utils.Config;
import datagenerator.Utils.ErrorHandling;
import datagenerator.Utils.RandomNumber;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

/**
 *
 * @author Mark
 */
public class Files {
    private static Map<Integer, Stop> stopList = null;
    private static ArrayList<NameSurname> nameList = null;
    private static HashMap<Long, Passenger> passengerDetailsMap = new HashMap<Long, Passenger>();
    
    /**
     * Read the GPSRoutes from the path provided
     * @param path
     * @return
     * @throws IOException
     */
    public static GPSRoutes readGPSDataSet(String path) throws IOException{
        
        //array used to hold the maze points
        GPSRoutes routes = new GPSRoutes();
        BufferedReader br = null;
        
        try {
            br = new BufferedReader(new FileReader(path));
            String line = br.readLine();
            
            while (line != null) {
                GPSRecord row = new GPSRecord(line);
                routes.addRow(row);
                line = br.readLine();
            }
        } catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        finally {
            if(br != null){
                br.close();
            }
        }
        return routes;
    }
    
    /**
     * Read and get the Stop list
     * @return
     * @throws IOException
     */
    public static Map<Integer, Stop> getStopList() throws IOException{
        if(stopList == null){
            stopList = new HashMap<Integer, Stop>();
            
            BufferedReader br = null;
            
            try {
                String path = Config.getPropertyString("stopListPath");
                
                br = new BufferedReader(new FileReader(path));
                String line = br.readLine();
                
                while (line != null) {
                    Stop stop = new Stop(line);
                    stopList.put(stop.getStopId(), stop);
                    line = br.readLine();
                }
            } catch(Exception ex){
                ErrorHandling.output(Level.SEVERE, ex);
            }
            finally {
                if(br != null){
                    br.close();
                }
            }
        }
        return stopList;
    }
    
    
    /**
     * Read and get the name and surname list
     * @return
     * @throws IOException
     */
    public static ArrayList<NameSurname> getNameSurnameList() throws IOException{
        if(nameList == null){
            nameList = new ArrayList<NameSurname>();
            
            BufferedReader br = null;
            
            try {
                String path = Config.getPropertyString("nameSurnameListPath");
                
                br = new BufferedReader(new FileReader(path));
                String line = br.readLine();
                
                while (line != null) {
                    nameList.add(new NameSurname(line));
                    line = br.readLine();
                }
            } catch(Exception ex){
                ErrorHandling.output(Level.SEVERE, ex);
            }
            finally {
                if(br != null){
                    br.close();
                }
            }
        }
        return nameList;
    }
    
    /**
     * method used to generate and output the data from memory to file
     * @param routes
     * @param seletedRouteList
     * @param biasList
     * @param outputName
     * @param separateRoutes
     * @param generatePassengerData
     * @param minPassenger
     * @param maxPassenger
     * @return
     * @throws IOException
     */
    public static String generate(GPSRoutes routes, List<Integer> seletedRouteList, BiasList biasList, String outputName,
            boolean separateRoutes,boolean generatePassengerData, int minPassenger,
            int maxPassenger) throws IOException{
        
        BufferedWriter output = null;
        
        String folderName = String.format("%s%s",
                Config.getPropertyString("writeSetDirectory"),
                outputName
        );
        
        String fileName = String.format("%s/%s",
                folderName,
                outputName
        );
        
        try {
            
            //make a new folder
            new File(folderName).mkdir();
            
            //output the file with stops
            if(Config.getPropertyBoolean("saveStopsToFile")){
                dumpStops(fileName);
            }
            
            if(!separateRoutes){
                dumpSingleOutput(
                        routes,
                        seletedRouteList,
                        biasList,
                        fileName,
                        generatePassengerData,
                        minPassenger,
                        maxPassenger
                );
            }else{
                dumpMultiOutput(
                        routes,
                        seletedRouteList,
                        biasList,
                        fileName,
                        generatePassengerData,
                        minPassenger,
                        maxPassenger
                );
            }
            
            dumpStopIdMap(
                    routes,
                    seletedRouteList,
                    fileName
            );
            
        } catch ( Exception ex ) {
            ErrorHandling.output(Level.SEVERE, ex);
        } finally {
            if ( output != null ) output.close();
        }
        return fileName;
    }
    
    /**
     * save the stops to file
     * @param fileName
     * @throws IOException
     */
    private static void dumpStops(String fileName) throws IOException{
        BufferedWriter output = null;
        try{
            //output the file with stops
            File stopsFile = new File(
                    String.format("%s_Stops%s",
                            fileName,
                            Config.getPropertyString("wirteSetExtension")
                    )
            );
            output = new BufferedWriter(new FileWriter(stopsFile));
            output.write(Stop.toStingTitles());
            
            for(Entry<Integer, Stop> stopEntry : getStopList().entrySet()){
                Stop stop = stopEntry.getValue();
                output.write(stop.toString());
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        } finally {
            if ( output != null ) output.close();
        }
    }
    
    /**
     * Method used to dump the map of bus stops per route selected
     * @param routes
     * @param seletedRouteList
     * @param fileName
     * @throws IOException
     */
    private static void dumpStopIdMap(GPSRoutes routes, List<Integer> seletedRouteList, String fileName) throws IOException{
        BufferedWriter output = null;
        try{
            //output the file with stops
            File stopsFile = new File(
                    String.format("%s_RouteStopsMap%s",
                            fileName,
                            Config.getPropertyString("wirteSetExtension")
                    )
            );
            output = new BufferedWriter(new FileWriter(stopsFile));
            output.write("Route Id,Stop Id\n");
            
            for(int routeId :seletedRouteList){
                GPSRoute route = routes.getRoute(routeId);
                ArrayList<Integer> stopList = route.getBusStopList();
                for(int stopId : stopList){
                    output.write(routeId + "," + stopId + "\n");
                }
            }
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        } finally {
            if ( output != null ) output.close();
        }
    }
    
    
    /**
     * save routes in a single file
     * @param routes
     * @param seletedRouteList
     * @param fileName
     * @throws IOException
     */
    private static void dumpSingleOutput(
            GPSRoutes routes, List<Integer> seletedRouteList, BiasList biasList, String fileName,
            boolean generatePassengerData, int minPassenger, int maxPassenger) throws IOException{
        
        BufferedWriter outputGps = null;
        BufferedWriter outputPassenger = null;
        try{
            File gpsFile = new File(
                    String.format("%s_GPSPoints%s",
                            fileName,
                            Config.getPropertyString("wirteSetExtension")
                    )
            );
            outputGps = new BufferedWriter(new FileWriter(gpsFile));
            outputGps.write(getRouteString(seletedRouteList));
            
            //if generating the passenger data create a file for it
            if(generatePassengerData){
                File passengerFile = new File(
                        String.format("%s_Passengers%s",
                                fileName,
                                Config.getPropertyString("wirteSetExtension")
                        )
                );
                outputPassenger = new BufferedWriter(new FileWriter(passengerFile));
                outputPassenger.write(getRouteString(seletedRouteList));
            }
            
            //save the headers
            dumpGPSAndPassengerFileTitles(outputGps, outputPassenger, generatePassengerData);
            
            //save the dataset points
            int passengerCardNumberIndex = 0;
            for(int routeId :seletedRouteList){
                GPSRoute route = routes.getRoute(routeId);
                
                for(GPSRecord row : route.getGPSRowList()){
                    
                    passengerCardNumberIndex = dumpData(
                            outputGps, outputPassenger, generatePassengerData,
                            minPassenger, maxPassenger, row, biasList,
                            passengerCardNumberIndex
                    );
                    
                }
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        } finally {
            if ( outputGps != null ) outputGps.close();
            if ( outputPassenger != null ) outputPassenger.close();
        }
    }
    
    /**
     * save routes in a multiple files based on the route number.
     * Each route is separated in an individual file
     * @param routes
     * @param seletedRouteList
     * @param fileName
     * @throws IOException
     */
    private static void dumpMultiOutput(
            GPSRoutes routes, List<Integer> seletedRouteList, BiasList biasList,
            String fileName, boolean generatePassengerData, int minPassenger,
            int maxPassenger
    ) throws IOException{
        
        BufferedWriter outputGps = null;
        BufferedWriter outputPassenger = null;
        try{
            int passengerCardNumberIndex = 0;
            for(int routeId :seletedRouteList){
                //get route
                GPSRoute route = routes.getRoute(routeId);
                
                //create route file name
                File gpsFile = new File(
                        String.format("%s_GPSPoints_R%d%s",
                                fileName,
                                route.getRouteID(),
                                Config.getPropertyString("wirteSetExtension")
                        )
                );
                outputGps = new BufferedWriter(new FileWriter(gpsFile));
                
                //if generating the passenger data create a file for it
                if(generatePassengerData){
                    File passengerFile = new File(
                            String.format("%s_Passengers_R%d%s",
                                    fileName,
                                    route.getRouteID(),
                                    Config.getPropertyString("wirteSetExtension")
                            )
                    );
                    outputPassenger = new BufferedWriter(new FileWriter(passengerFile));
                }
                
                //print the headers
                dumpGPSAndPassengerFileTitles(outputGps, outputPassenger, generatePassengerData);
                
                //print points
                for(GPSRecord row : route.getGPSRowList()){
                    passengerCardNumberIndex = dumpData(
                            outputGps, outputPassenger, generatePassengerData,
                            minPassenger, maxPassenger, row, biasList,
                            passengerCardNumberIndex
                    );
                }
                
                //close the BufferedWriter each time
                outputGps.close();
                if ( outputPassenger != null ) outputPassenger.close();
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        } finally {
            if ( outputGps != null ) outputGps.close();
            if ( outputPassenger != null ) outputPassenger.close();
        }
    }
    
    /**
     * method used to save the headers of the dataset to files.
     * @param outputGps
     * @param outputPassenger
     * @param generatePassengerData
     * @throws IOException
     */
    private static void dumpGPSAndPassengerFileTitles(
            BufferedWriter outputGps, BufferedWriter outputPassenger, boolean generatePassengerData
    ) throws IOException{
        //output format
        switch(Config.getPropertyInt("outputFormat")){
            case 1:{
                outputGps.write(GPSRecord.toStringFullTitles());
                if(generatePassengerData){
                    outputPassenger.write(Passenger.toStringFullTitles());
                }
                break;
            }
            case 2:{
                outputGps.write(GPSRecord.toStringShortTitles());
                if(generatePassengerData){
                    outputPassenger.write(Passenger.toStringShortTitles());
                }
                break;
            }
            case 0:
            default:{
                outputGps.write(GPSRecord.toStringTtiles());
                if(generatePassengerData){
                    outputPassenger.write(Passenger.toStringTitles());
                }
                break;
            }
        }
    }
    
    /**
     * method used to save each row of the dataset to files.
     * It dumps gps, passenger and bias data
     * @param outputGps
     * @param outputPassenger
     * @param generatePassengerData
     * @param minPassenger
     * @param maxPassenger
     * @param row
     * @param passengerCardNumberIndex
     */
    private static int dumpData(
            BufferedWriter outputGps,
            BufferedWriter outputPassenger,
            boolean generatePassengerData,
            int minPassenger,
            int maxPassenger,
            GPSRecord row,
            BiasList biasList,
            int passengerCardNumberIndex
    ){
        try{
            //output format
            switch(Config.getPropertyInt("outputFormat")){
                case 1:{
                    outputGps.write(row.toStringFull());
                    if(generatePassengerData){
                        passengerCardNumberIndex = dumpPassengers(row, minPassenger, maxPassenger, passengerCardNumberIndex, outputPassenger);
                        
                        
//                        int rand = RandomNumber.getRandomInt(minPassenger, maxPassenger);
//                        for(int i=0;i<rand;i++){
//                            Passenger passenger = new Passenger(passengerCardNumberIndex, row);
//                            passenger.updateTimeStamp(i);
//                            outputPassenger.write(passenger.toStringFull());
//                            passengerCardNumberIndex++;
//                        }
                    }
                    break;
                }
                case 2:{
                    outputGps.write(row.toStringShort());
                    if(generatePassengerData){
                        passengerCardNumberIndex = dumpPassengers(row, minPassenger, maxPassenger, passengerCardNumberIndex, outputPassenger);
                        
//                        int rand = RandomNumber.getRandomInt(minPassenger, maxPassenger);
//                        for(int i=0;i<rand;i++){
//                            Passenger passenger = new Passenger(passengerCardNumberIndex, row);
//                            passenger.updateTimeStamp(i);
//                            outputPassenger.write(passenger.toStringShort());
//                            passengerCardNumberIndex++;
//                        }
                    }
                    break;
                }
                case 0:
                default:{
                    outputGps.write(row.toString());
                    if(generatePassengerData){
                        passengerCardNumberIndex = dumpPassengers(row, minPassenger, maxPassenger, passengerCardNumberIndex, outputPassenger);
//                        int rand = RandomNumber.getRandomInt(minPassenger, maxPassenger);
//                        for(int i=0;i<rand;i++){
//                            Passenger passenger = new Passenger(passengerCardNumberIndex, row);
//                            passenger.updateTimeStamp(i);
//                            outputPassenger.write(passenger.toString());
//                            passengerCardNumberIndex++;
//                        }
                    }
                    break;
                }
            }
            
            //dump biases
            passengerCardNumberIndex = dumpBiases(
                    outputGps,
                    outputPassenger,
                    generatePassengerData,
                    row,
                    biasList,
                    passengerCardNumberIndex,
                    maxPassenger
            );
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
        return passengerCardNumberIndex;
    }
    
    private static int dumpPassengers(GPSRecord row, int minPassenger, int maxPassenger, int passengerCardNumberIndex, BufferedWriter outputPassenger) throws IOException{
        int rand = RandomNumber.getRandomInt(minPassenger, maxPassenger);
        int randFromGUI = 70;
        long secondBusTripTimeWindowLimt = 3600000000L;
        
        //randomly generate passengers based on the input
        for(int i=0;i<rand;i++){
            Passenger passenger = null;
            
            //randomly generate passengers which recomute
            long numOfUniquePassengers = passengerDetailsMap.size();
            
            //check if there are at least 100 passengers and randomly put re-commuting passingers
            if(numOfUniquePassengers > 100 && RandomNumber.getRandomInt(0, 100) > (100-randFromGUI)){
                
                //get a random passenger number from the details list
                long getRandomPassengerId = RandomNumber.getRandomLong(0L, numOfUniquePassengers);
                Passenger passengerFromList = passengerDetailsMap.get(getRandomPassengerId);
                
                passenger = new Passenger(passengerFromList, row, secondBusTripTimeWindowLimt-(i*1000000));
            }
            
            //if the parameters are not met, a new passenger is created
            else{
                passenger = new Passenger(passengerCardNumberIndex, row);
                
                //store the details of this passenger
                if(!passengerDetailsMap.containsKey(passenger.getCardNumber())){
                    passengerDetailsMap.put(passenger.getCardNumber(), passenger);
                }
                
                passengerCardNumberIndex++;
            }
            
            passenger.updateTimeStamp(i);
            
            switch(Config.getPropertyInt("outputFormat")){
                case 1:{
                    outputPassenger.write(passenger.toStringFull());
                    break;
                }
                case 2:{
                    outputPassenger.write(passenger.toStringShort());
                    break;
                }
                case 0:
                default:{
                    outputPassenger.write(passenger.toString());
                    break;
                }
            }
            
        }
        
        return passengerCardNumberIndex;
    }
    
    /**
     * This method is used to dump a bias for each particular route/stop
     * It must be called when looping and dumping the GPS dataset
     * It has 2 functionalities;
     * -when a timestamp is inputed, only that records which matches the route id, stop id and timestamp has a bias
     * -when no timestamp is inputed, all records which have the same route id and stop id will get a bias
     * This method performs dumping by invoking <dumpBias> method
     * @param outputGps
     * @param outputPassenger
     * @param generatePassengerData
     * @param row
     * @param biasList
     * @param passengerCardNumberIndex
     * @return
     * @throws IOException
     */
    private static int dumpBiases(
            BufferedWriter outputGps,
            BufferedWriter outputPassenger,
            boolean generatePassengerData,
            GPSRecord row,
            BiasList biasList,
            int passengerCardNumberIndex,
            int maxPassenger
    ) throws IOException{
        try{
            
            for(Entry<Long, Bias> biasEntry : biasList.getBiasMap().entrySet()){
                Bias bias = biasEntry.getValue();
                
                //check if the route id and bias id match
                if(row.getStopID() == bias.getStopId() && row.getLineID() == bias.getRouteId()){
                    
                    //check if a timestamp was inputed by the user
                    //if a time stamp is entered by the user, create a bias only for that timestamp
                    if(bias.getTimeStamp() > 0){
                        //check that the timestamp matches
                        if(row.getTimeStamp() == bias.getTimeStamp()){
                            passengerCardNumberIndex = dumpBias(outputGps, outputPassenger, generatePassengerData, row, bias, passengerCardNumberIndex, maxPassenger);
                        }
                    }
                    //perform for all stops with the bias id and stop id
                    else{
                        passengerCardNumberIndex = dumpBias(outputGps, outputPassenger, generatePassengerData, row, bias, passengerCardNumberIndex, maxPassenger);
                    }
                }
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
        return passengerCardNumberIndex;
    }
    
    /**
     * This method is used to dump each individual bias.
     * It should be called when a bias needs to be dumped, it is the actual method that dumps a bias to file
     * It takes in consideration the output format
     * @param outputGps
     * @param outputPassenger
     * @param generatePassengerData
     * @param row
     * @param bias
     * @param passengerCardNumberIndex
     * @return
     */
    private static int dumpBias(
            BufferedWriter outputGps,
            BufferedWriter outputPassenger,
            boolean generatePassengerData,
            GPSRecord row,
            Bias bias,
            int passengerCardNumberIndex,
            int maxPassenger
    ){
        try{
            //if dumping gps data is turned on from the gui
            if(bias.isDumpGPSData()){
                int randNumberOfGPS = RandomNumber.getRandomInt(bias.getMinGps(), bias.getMaxGps());
                for(int i=0;i<randNumberOfGPS;i++){
                    switch(Config.getPropertyInt("outputFormat")){
                        case 1:{
                            outputGps.write(row.toStringFull());
                            break;
                        }
                        case 2:{
                            outputGps.write(row.toStringShort());
                            break;
                        }
                        case 0:
                        default:{
                            outputGps.write(row.toString());
                            break;
                        }
                        
                    }
                }
            }
            
            //if generate passenger data is turned on from the gui(both main window and bias window)
            if(bias.isDumpPassengerData() && generatePassengerData){
                int randNumberOfPassenger = RandomNumber.getRandomInt(bias.getMinPassenger(), bias.getMaxPassenger());
                for(int i=0;i<randNumberOfPassenger;i++){
                    switch(Config.getPropertyInt("outputFormat")){
                        case 1:{
                            Passenger passenger = new Passenger(passengerCardNumberIndex, row);
                            passenger.updateTimeStamp(i+maxPassenger);
                            outputPassenger.write(passenger.toStringFull());
                            passengerCardNumberIndex++;
                            break;
                        }
                        case 2:{
                            Passenger passenger = new Passenger(passengerCardNumberIndex, row);
                            passenger.updateTimeStamp(i+maxPassenger);
                            outputPassenger.write(passenger.toStringShort());
                            passengerCardNumberIndex++;
                            break;
                        }
                        case 0:
                        default:{
                            Passenger passenger = new Passenger(passengerCardNumberIndex, row);
                            passenger.updateTimeStamp(i+maxPassenger);
                            outputPassenger.write(passenger.toString());
                            passengerCardNumberIndex++;
                            break;
                        }
                    }
                }
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
        return passengerCardNumberIndex;
    }
    
    /**
     * Return a string with all the route numbers
     * @param seletedRouteList
     * @return
     */
    private static String getRouteString(List<Integer> seletedRouteList){
        String output = "Routes In This Set: ";
        try{
            int index = 0;
            int size = seletedRouteList.size();
            
            for(int routeId :seletedRouteList){
                output += routeId;
                
                if(index < size-1){
                    output += ", ";
                }
                index++;
            }
            
            output += "\n";
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        
        return output;
    }
}
