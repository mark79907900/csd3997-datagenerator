/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.GUI;

import datagenerator.GUI.Components.CheckBox;
import datagenerator.GUI.Components.ListBox;
import datagenerator.GUI.Components.Panel;
import datagenerator.GUI.Components.TextBox;
import datagenerator.GUI.Validations.ValidationState;
import datagenerator.Types.Bias;
import datagenerator.Types.BiasList;
import datagenerator.Types.GPSRoutes;
import datagenerator.Utils.Config;
import datagenerator.Utils.ErrorHandling;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.logging.Level;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author Mark
 */
public class AddBiasGUI extends JFrame{
    private int selectedRoute;
    private GPSRoutes routes;
    private BiasList biasList;
    private JPanel topPanel;
    private JPanel buttonsPanel;
    private boolean generatePassengerData;
    private Font font;
    private AddBiasGUI self;
    
    public AddBiasGUI(BiasList biasList, GPSRoutes routes, int selectedRoute,boolean generatePassengerData, Font font) {
        super(String.format(Config.getPropertyString("addBiasWindowName"), selectedRoute));
        this.selectedRoute = selectedRoute;
        this.routes = routes;
        this.biasList = biasList;
        this.font = font;
        this.generatePassengerData = generatePassengerData;
        this.self = this;
        setLayout(new BorderLayout());
        
        
        //set the defulat minimum window size
        setSize(
                Config.getPropertyInt("addBiasWindowWidth"),
                Config.getPropertyInt("addBiasWindowHeight")
        );
        
        
        //center of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        setResizable(false);
        
        //disable main window
        MainWindow.getMainWindow().setEnabled(false);
        
        //re-enable main window when this windows is closed
        addWindowListener(new WindowListener() {
            
            @Override
            public void windowOpened(WindowEvent e) {
            }
            
            @Override
            public void windowClosing(WindowEvent e) {
                MainWindow.getMainWindow().setEnabled(true);
            }
            
            @Override
            public void windowClosed(WindowEvent e) {
            }
            
            @Override
            public void windowIconified(WindowEvent e) {
            }
            
            @Override
            public void windowDeiconified(WindowEvent e) {
            }
            
            @Override
            public void windowActivated(WindowEvent e) {
            }
            
            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });
        
        setup();
    }
    
    /**
     * private method which loads the window gui
     */
    private void setup(){
        this.topPanel = new Panel("", "", Panel.panelType.GridBagLayout).getPanel();
        topPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        ListBox busStops = new ListBox(topPanel, 0,
                Config.getPropertyString("addBiasBusStopsLabel"),
                Config.getPropertyString("addBiasBusStopsToolTip"),
                font
        );
        busStops.updateList(routes.getRoute(selectedRoute).getBusStopObjectArray());
        busStops.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        CheckBox gPSDataCheckBox = new CheckBox(topPanel, 1,
                Config.getPropertyString("addBiasGPSDataCheckBoxLabel"),
                Config.getPropertyString("addBiasGPSDataCheckBoxToolTip"),
                font
        );
        
        CheckBox passengerDataCheckBox = new CheckBox(topPanel, 2,
                Config.getPropertyString("addBiasPassengerDataCheckBoxLabel"),
                Config.getPropertyString("addBiasPassengerDataCheckBoxToolTip"),
                font
        );
        passengerDataCheckBox.setEnabled(generatePassengerData);
        
        TextBox timeStamp = new TextBox(topPanel, 3,
                Config.getPropertyString("addBiasTimeStampLabel"),
                Config.getPropertyString("addBiasTimeStampToolTip"),
                font
        );
        
        TextBox minNumberOfGPSPoints = new TextBox(topPanel, 4,
                Config.getPropertyString("addBiasMinNumberOfGPSPointsLabel"),
                Config.getPropertyString("addBiasMinNumberOfGPSPointsToolTip"),
                font
        );
        minNumberOfGPSPoints.setEnabled(false);
        
        
        TextBox maxNumberOfGPSPoints = new TextBox(topPanel, 5,
                Config.getPropertyString("addBiasMaxNumberOfGPSPointsLabel"),
                Config.getPropertyString("addBiasMaxNumberOfGPSPointsToolTip"),
                font
        );
        maxNumberOfGPSPoints.setEnabled(false);
        
        TextBox minNumberOfPassengerPoints = new TextBox(topPanel, 6,
                Config.getPropertyString("addBiasMinNumberOfPassengerPointsLabel"),
                Config.getPropertyString("addBiasMinNumberOfPassengerPointsToolTip"),
                font
        );
        minNumberOfPassengerPoints.setEnabled(false);
        
        TextBox maxNumberOfPassengerPoints = new TextBox(topPanel, 7,
                Config.getPropertyString("addBiasMaxNumberOfPassengerPointsLabel"),
                Config.getPropertyString("addBiasMaxNumberOfPassengerPointsToolTip"),
                font
        );
        maxNumberOfPassengerPoints.setEnabled(false);
        
        //enable/disable fields related to the GPS data
        gPSDataCheckBox.getCheckBox().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED){
                    minNumberOfGPSPoints.setEnabled(true);
                    maxNumberOfGPSPoints.setEnabled(true);
                }else{
                    minNumberOfGPSPoints.setEnabled(false);
                    maxNumberOfGPSPoints.setEnabled(false);
                }
            }
        });
        
        //enable/disable fields related to the passenger data
        passengerDataCheckBox.getCheckBox().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED){
                    minNumberOfPassengerPoints.setEnabled(true);
                    maxNumberOfPassengerPoints.setEnabled(true);
                }
                else{
                    minNumberOfPassengerPoints.setEnabled(false);
                    maxNumberOfPassengerPoints.setEnabled(false);
                }
            }
        });
        
        this.add(this.topPanel, BorderLayout.NORTH);
        
        this.buttonsPanel = new Panel("", "", Panel.panelType.FlowLayout).getPanel();
        
        //close button
        JButton cancel = new JButton(Config.getPropertyString("addBiasCancelLabel"));
        cancel.setToolTipText(Config.getPropertyString("addBiasCancelToolTip"));
        cancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    MainWindow.getMainWindow().setEnabled(true);
                    self.dispose();
                } catch (Exception ex) {
                    ErrorHandling.output(Level.SEVERE, ex);
                }
            }
        });
        this.buttonsPanel.add(cancel);
        
        //close button
        JButton save = new JButton(Config.getPropertyString("addBiasSaveLabel"));
        save.setToolTipText(Config.getPropertyString("addBiasSaveToolTip"));
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if(gPSDataCheckBox.isSelected() || passengerDataCheckBox.isSelected()){
                        int minGPS = 0, maxGPS = 0, minPassenger = 0, maxPassenger = 0;
                        long time = 0;
                        boolean error = false;
                        
                        ValidationState minGPSValidation = Validations.validation(
                                minNumberOfGPSPoints,
                                gPSDataCheckBox,
                                self,
                                Config.getPropertyString("addBiasMinNumberOfGPSPointsMessage")
                        );
                        
                        if(minGPSValidation == ValidationState.VALID){
                            minGPS = Integer.parseInt(minNumberOfGPSPoints.getText());
                        }else if (minGPSValidation == ValidationState.INVALID){
                            error = true;
                        }
                        
                        ValidationState maxGPSValidation = Validations.validation(
                                maxNumberOfGPSPoints,
                                gPSDataCheckBox,
                                self,
                                Config.getPropertyString("addBiasMaxNumberOfGPSPointsMessage")
                        );
                        
                        if(maxGPSValidation == ValidationState.VALID){
                            maxGPS = Integer.parseInt(maxNumberOfGPSPoints.getText());
                        }else if (maxGPSValidation == ValidationState.INVALID){
                            error = true;
                        }
                        
                        
//                        if(!maxNumberOfGPSPoints.getText().isEmpty()){
//                            maxGPS = Integer.parseInt(maxNumberOfGPSPoints.getText());
//                        }else{
//                            if(gPSDataCheckBox.isSelected()){
//                                JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiasMaxNumberOfGPSPointsMessage"),
//                                        "Inane error",
//                                        JOptionPane.ERROR_MESSAGE);
//                                error = true;
//                            }
//                        }
                        
                        ValidationState minPassengerValidation = Validations.validation(
                                minNumberOfPassengerPoints,
                                passengerDataCheckBox,
                                self,
                                Config.getPropertyString("addBiasMinNumberOfPassengerPointsMessage")
                        );
                        
                        if(minPassengerValidation == ValidationState.VALID){
                            minPassenger = Integer.parseInt(minNumberOfPassengerPoints.getText());
                        }else if (minPassengerValidation == ValidationState.INVALID){
                            error = true;
                        }
                        
//                        if(!minNumberOfPassengerPoints.getText().isEmpty()){
//                            minPassenger = Integer.parseInt(minNumberOfPassengerPoints.getText());
//                        }else{
//                            if(passengerDataCheckBox.isSelected()){
//                                JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiasMinNumberOfPassengerPointsMessage"),
//                                        "Inane error",
//                                        JOptionPane.ERROR_MESSAGE);
//                                error = true;
//                            }
//                        }
                        
                        ValidationState maxPassengerValidation = Validations.validation(
                                maxNumberOfPassengerPoints,
                                passengerDataCheckBox,
                                self,
                                Config.getPropertyString("addBiasmaxNumberOfPassengerPointsMessage")
                        );
                        
                        if(maxPassengerValidation == ValidationState.VALID){
                            maxPassenger = Integer.parseInt(maxNumberOfPassengerPoints.getText());
                        }else if (maxPassengerValidation == ValidationState.INVALID){
                            error = true;
                        }
                        
//                        if(!maxNumberOfPassengerPoints.getText().isEmpty()){
//                            maxPassenger = Integer.parseInt(maxNumberOfPassengerPoints.getText());
//                        }else{
//                            if(passengerDataCheckBox.isSelected()){
//                                JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiasmaxNumberOfPassengerPointsMessage"),
//                                        "Inane error",
//                                        JOptionPane.ERROR_MESSAGE);
//                                error = true;
//                            }
//                        }
                        
                        if(!busStops.getSelected().isEmpty()){
                            ValidationState timeStampValidation = Validations.warning(
                                    timeStamp,
                                    self,
                                    String.format(
                                            Config.getPropertyString("addBiastimeStampMessage"),
                                            selectedRoute,
                                            (int)busStops.getSelected().get(0)
                                    )
                            );
                            
                            if(timeStampValidation == ValidationState.VALID){
                                time = Long.parseLong(timeStamp.getText());
                            }else if (timeStampValidation == ValidationState.INVALID){
                                time = 0;
                            }
                        }
                        
                        
//                        if(!timeStamp.getText().isEmpty()){
//                            time = Long.parseLong(timeStamp.getText());
//                        }else{
//                            JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiastimeStampMessage"),
//                                    "Inane error",
//                                    JOptionPane.ERROR_MESSAGE);
//                            error = true;
//                        }
                        
                        if(busStops.getSelected().size() < 1){
                            JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiasSelectBusStopMessage"),
                                    "Inane error",
                                    JOptionPane.ERROR_MESSAGE);
                            error = true;
                        }
                        
                        if(!error){
                            //save
                            biasList.add(new Bias(selectedRoute,
                                    (int)busStops.getSelected().get(0),
                                    minGPS,
                                    maxGPS,
                                    minPassenger,
                                    maxPassenger,
                                    time,
                                    gPSDataCheckBox.isSelected(),
                                    passengerDataCheckBox.isSelected()
                            ));
                            
                            //close window
                            MainWindow.getMainWindow().setEnabled(true);
                            self.dispose();
                        }
                        
                    }else{
                        JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiasAtLeastOneSelectedMessage"),
                                "Inane error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } catch (Exception ex) {
                    ErrorHandling.output(Level.SEVERE, ex);
                }
            }
        });
        this.buttonsPanel.add(save);
        
        this.add(this.buttonsPanel, BorderLayout.SOUTH);
    }
    
    /***
     * Set the Frame to visible
     */
    public void setVisible(){
        setVisible(true);
    }
}
