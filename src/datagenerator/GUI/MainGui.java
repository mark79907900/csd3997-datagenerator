package datagenerator.GUI;

import datagenerator.Files.Files;
import datagenerator.GUI.Components.Button;
import datagenerator.GUI.Components.CheckBox;
import datagenerator.GUI.Components.ComboBox;
import datagenerator.GUI.Components.FileChooser;
import datagenerator.GUI.Components.IButton;
import datagenerator.GUI.Components.IFileChooser;
import datagenerator.GUI.Components.ListBox;
import datagenerator.GUI.Components.Panel;
import datagenerator.GUI.Components.TextBox;
import datagenerator.GUI.Validations.ValidationState;
import datagenerator.Types.Bias;
import datagenerator.Types.BiasList;
import datagenerator.Types.GPSRoutes;
import datagenerator.Utils.Config;
import datagenerator.Utils.ErrorHandling;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Mark
 */
public class MainGui implements IFileChooser, IButton{
    private static MainGui self = null;
    
    private final MainWindow mw;
    private JPanel dataSetPanel;
    private JPanel biasPanel;
    private Font smallFont;
    private Font largeFont;
    private JPanel buttonsPannel;
    private GPSRoutes routes;
    private BiasList biasList;
    
    private ListBox routeNumbers;
    private ComboBox biasBusRoutes;
    private Button addBias;
    private Button removeBias;
    private CheckBox generatePassengerData;
    private CheckBox separeateRoutes;
    private TextBox minPassengerPerStop;
    private TextBox maxPassengerPerStop;
    
    /**
     * This method is used to init the GUI and ensure that only one instance can be created (Singleton)
     * @param callBack
     */
    public static void init(){
        if (self == null){
            self = new MainGui();
        }
    }
    
    /**
     * private Constructor to build the GUI
     * It creates a new Window and sets up the GUI components by invoking the private method setup
     * @param callBack
     */
    private MainGui() {
        this.mw = MainWindow.getMainWindow(Config.getPropertyString("mainWindowName"));
        this.smallFont = new Font("Serif", Font.PLAIN, 14);
        this.largeFont = new Font("Serif", Font.PLAIN, 20);
        this.routes = null;
        this.biasList = new BiasList();
        setup();
    }
    
    /**
     * Creates the Components
     * @param callBack
     */
    private void setup(){
        try{
            //Make the top panel which will hold the algorithm selection
            this.dataSetPanel = new Panel(
                    Config.getPropertyString("dataSetTitle"),
                    Config.getPropertyString("dataSetToolTip"),
                    Panel.panelType.GridBagLayout
            ).getPanel();
            
            FileChooser datasetFileChooser = new FileChooser(this, mw, dataSetPanel, 0,
                    Config.getPropertyString("datasetFileChooserLabel"),
                    Config.getPropertyString("datasetFileChooserToolTip"),
                    Config.getPropertyString("datasetFileChooserButtonToolTip"),
                    smallFont
            );
            
            TextBox outputName = new TextBox(dataSetPanel, 1,
                    Config.getPropertyString("outputNameLabel"),
                    Config.getPropertyString("outputNameToolTip"),
                    smallFont
            );
            
            routeNumbers = new ListBox(dataSetPanel, 2,
                    Config.getPropertyString("routeNumberLabel"),
                    Config.getPropertyString("routeNumberToolTip"),
                    smallFont
            );
            
            separeateRoutes = new CheckBox(dataSetPanel, 3,
                    Config.getPropertyString("separeteRoutesLabel"),
                    Config.getPropertyString("separeteRoutesToolTip"),
                    smallFont
            );
            
            generatePassengerData = new CheckBox(dataSetPanel, 4,
                    Config.getPropertyString("generatePassengerDataLabel"),
                    Config.getPropertyString("generatePassengerDataToolTip"),
                    smallFont
            );
            
            minPassengerPerStop = new TextBox(dataSetPanel, 5,
                    Config.getPropertyString("minPassengerPerStopLabel"),
                    Config.getPropertyString("minPassengerPerStopToolTip"),
                    smallFont
            );
            minPassengerPerStop.setEnabled(false);
            
            maxPassengerPerStop = new TextBox(dataSetPanel, 6,
                    Config.getPropertyString("maxPassengerPerStopLabel"),
                    Config.getPropertyString("maxPassengerPerStopToolTip"),
                    smallFont
            );
            maxPassengerPerStop.setEnabled(false);
            
            /*add listener to enable/disable minPassengerPerStop and maxPassenger
            PerStop based on the selection of generatePassengerData*/
            generatePassengerData.getCheckBox().addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    if(e.getStateChange() == ItemEvent.SELECTED){
                        minPassengerPerStop.setEnabled(true);
                        maxPassengerPerStop.setEnabled(true);
                    }else{
                        minPassengerPerStop.setEnabled(false);
                        maxPassengerPerStop.setEnabled(false);
                    }
                }
            });
            
            //add the panel to the main window
            this.mw.add(this.dataSetPanel, BorderLayout.NORTH);
            
            //make the bottom panel which will hold the file selection
            this.biasPanel = new Panel(
                    Config.getPropertyString("biasTitle"),
                    Config.getPropertyString("biasToolTip"),
                    Panel.panelType.GridBagLayout
            ).getPanel();
            
            biasBusRoutes = new ComboBox(biasPanel, 0,
                    Config.getPropertyString("biasBusRoutesLabel"),
                    Config.getPropertyString("biasBusRoutesToolTip"),
                    smallFont
            );
            
            addBias = new Button(this, biasPanel, 1,
                    Config.getPropertyString("addBiasLabel"),
                    Config.getPropertyString("addBiasButtonLabel"),
                    Config.getPropertyString("addBiasToolTip"),
                    smallFont
            );
            addBias.setButtonSize(new Dimension(45,25));
            
            removeBias = new Button(this, biasPanel, 2,
                    Config.getPropertyString("removeBiasLabel"),
                    Config.getPropertyString("removeBiasButtonLabel"),
                    Config.getPropertyString("removeBiasToolTip"),
                    smallFont
            );
            removeBias.setButtonSize(new Dimension(45,25));
            
            //actionListener which changes the values when selecting different items from the list
            routeNumbers.getListBox().addListSelectionListener(new ListSelectionListener() {
                @Override
                public void valueChanged(ListSelectionEvent e) {
                    JList routeNumbersList = (JList)e.getSource();
                    if(routeNumbersList.getSelectedValuesList() != null){
                        biasBusRoutes.updateList(
                                routeNumbersList.getSelectedValuesList().toArray()
                        );
                    }
                }
            });
            
            //add the panel to the main window
            this.mw.add(this.biasPanel, BorderLayout.CENTER);
            
            //make the bottom panel which will hold the file selection
            this.buttonsPannel = new Panel(
                    "",
                    "",
                    Panel.panelType.FlowLayout
            ).getPanel();
            
            //close button
            JButton close = new JButton(Config.getPropertyString("closeButtonLabel"));
            close.setToolTipText(Config.getPropertyString("closeButtonToolTip"));
            close.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        mw.dispose();
                    } catch (Exception ex) {
                        ErrorHandling.output(Level.SEVERE, ex);
                    }
                }
            });
            this.buttonsPannel.add(close);
            
            //load button
            JButton load = new JButton(Config.getPropertyString("loadDataSetButtonLabel"));
            load.setToolTipText(Config.getPropertyString("loadDataSetButtonToolTip"));
            load.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        String path = datasetFileChooser.getPath();
                        
                        //if no dataset is selected notify the user
                        if(path == null){
                            JOptionPane.showMessageDialog(mw, Config.getPropertyString("datasetNotSelectedMessage"),
                                    "Inane error",
                                    JOptionPane.ERROR_MESSAGE);
                        }
                        
                        //if a dataset is selected process it and update lists
                        else{
                            //load the routes data for the given dataset path
                            routes = Files.readGPSDataSet(
                                    path
                            );
                            
                            //update route number list box
                            routeNumbers.updateList(
                                    routes.getRouteNumbersObjects()
                            );
                            
//                            biasBusRoutes.updateList(
//                                    routes.getRouteNumbersObjects()
//                            );
                        }
                        
                    } catch (IOException ex) {
                        ErrorHandling.output(Level.SEVERE, ex);
                    }
                }
            });
            this.buttonsPannel.add(load);
            
            //load button
            JButton generate = new JButton(Config.getPropertyString("generateButtonLabel"));
            generate.setToolTipText(Config.getPropertyString("generateButtonToolTip"));
            generate.addActionListener(new ActionListener() {
                
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        boolean error = false;
                        String name = "";
                        int minPassenger = 0, maxPassenger =0;
                        List<Integer> seletedRouteList = (List<Integer>)routeNumbers.getSelected();
                        
                        ValidationState dataSetLoaded = Validations.validation(
                                routes,
                                mw,
                                Config.getPropertyString("datasetNotSelectedAndLoadedMessage")
                        );
                        
                        ValidationState outputNameValidation = Validations.validation(
                                outputName,
                                mw,
                                Config.getPropertyString("outputNameEmptyMessage")
                        );
                        
                        ValidationState minPassengerValidation = Validations.validation(
                                minPassengerPerStop,
                                generatePassengerData,
                                mw,
                                Config.getPropertyString("minPassengerValidationMessage")
                        );
                        
                        ValidationState maxPassengerValidation = Validations.validation(
                                maxPassengerPerStop,
                                generatePassengerData,
                                mw,
                                Config.getPropertyString("maxPassengerValidationMessage")
                        );
                        
                        ValidationState seletedRouteValidation = Validations.validation(
                                seletedRouteList,
                                mw,
                                Config.getPropertyString("seletedRouteValidationMessage")
                        );
                        
                        
                        if(dataSetLoaded == ValidationState.INVALID){
                            error = true;
                        }
                        
                        if(outputNameValidation == ValidationState.VALID){
                            name = outputName.getText();
                        }else{
                            error = true;
                        }
                        
                        if(minPassengerValidation == ValidationState.VALID){
                            minPassenger = Integer.parseInt(minPassengerPerStop.getText());
                        }else if (minPassengerValidation == ValidationState.INVALID){
                            error = true;
                        }
                        
                        if(maxPassengerValidation == ValidationState.VALID){
                            maxPassenger = Integer.parseInt(maxPassengerPerStop.getText());
                        }else if(maxPassengerValidation == ValidationState.INVALID){
                            error = true;
                        }
                        
                        if(seletedRouteValidation == ValidationState.INVALID){
                            error = true;
                        }
                        
                        if(!error){
                            Files.generate(
                                    routes,
                                    seletedRouteList,
                                    biasList,
                                    name,
                                    separeateRoutes.isSelected(),
                                    generatePassengerData.isSelected(),
                                    minPassenger,
                                    maxPassenger
                            );
                        }
                    } catch (Exception ex) {
                        ErrorHandling.output(Level.SEVERE, ex);
                    }
                }
            });
            this.buttonsPannel.add(generate);
            
            //add the panel to the main window
            this.mw.add(this.buttonsPannel, BorderLayout.SOUTH);
            
            //set the window visiable
            this.mw.setVisible();
            this.mw.setResizable(false);
            
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
    @Override
    public void removeAndResetDataSet() {
        routes = null;
        routeNumbers.clearList();
        biasBusRoutes.clearList();
    }
    
    @Override
    public void action(JButton button) {
        try{
            if(button.equals(addBias.getButton())){
                if(biasBusRoutes.getSelected() != null){
                    int selectedRoute = (int)biasBusRoutes.getSelected();
                    
                    AddBiasGUI addBiasWindow = new AddBiasGUI(biasList, routes, selectedRoute, generatePassengerData.isSelected(), smallFont);
                    addBiasWindow.setVisible();
                }else{
                    JOptionPane.showMessageDialog(mw, Config.getPropertyString("datasetNotSelectedAndLoadedMessage"),
                            "Inane error",
                            JOptionPane.ERROR_MESSAGE);
                }
                
            }else if(button.equals(removeBias.getButton())){
                if(routes == null){
                    JOptionPane.showMessageDialog(mw, Config.getPropertyString("datasetNotSelectedAndLoadedMessage"),
                            "Inane error",
                            JOptionPane.ERROR_MESSAGE);
                }else if(biasList.isEmpty()){
                    JOptionPane.showMessageDialog(mw, Config.getPropertyString("biasListIsEmpty"),
                            "Inane error",
                            JOptionPane.ERROR_MESSAGE);
                }
                else{
                    RemoveBiasGUI removeBiasGUI = new RemoveBiasGUI(biasList, routes, generatePassengerData.isSelected(), smallFont);
                    removeBiasGUI.setVisible();
                }
            }else{
                ErrorHandling.throwException(String.format("This button <%s> is not yet implemented", button.getText()));
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
    }
    
}
