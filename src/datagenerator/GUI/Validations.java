/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.GUI;

import datagenerator.GUI.Components.CheckBox;
import datagenerator.GUI.Components.TextBox;
import datagenerator.Utils.ErrorHandling;
import java.awt.Component;
import java.util.logging.Level;
import javax.swing.JOptionPane;

/**
 * This is a static class which contains validations
 * @author Mark
 */
public class Validations {
    
    public enum ValidationState{VALID,INVALID,DONOTCHECK};
    
    /**
     * validation which checks if the text box is empty and is dependent on a checkbox status
     * @param textbox
     * @param checkBox
     * @param component
     * @param ErrorMessage
     * @return
     */
    public static ValidationState validation(TextBox textbox, CheckBox checkBox, Component component, String ErrorMessage){
        ValidationState valid = ValidationState.INVALID;
        
        try{
            if(!textbox.getText().isEmpty()){
                valid = ValidationState.VALID;
            }else{
                //check if the check box is ticked
                if(checkBox.isSelected()){
                    JOptionPane.showMessageDialog(
                            component,
                            ErrorMessage,
                            "Inane error",
                            JOptionPane.ERROR_MESSAGE
                    );
                }
                //if the check box is ticked, the validation is not needed
                else{
                    valid = ValidationState.DONOTCHECK;
                }
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return valid;
    }
    
    
    /**
     * validation which checks if the text box is empty
     * @param textbox
     * @param component
     * @param ErrorMessage
     * @return
     */
    public static ValidationState validation(TextBox textbox, Component component, String ErrorMessage){
        ValidationState valid = ValidationState.INVALID;
        try{
            if(!textbox.getText().isEmpty()){
                valid = ValidationState.VALID;
            }else{
                JOptionPane.showMessageDialog(component, ErrorMessage,
                        "Inane error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return valid;
    }
    
    public static ValidationState validation(Object object, Component component, String ErrorMessage){
        ValidationState valid = ValidationState.INVALID;
        try{
            if(object != null){
                valid = ValidationState.VALID;
            }else{
                JOptionPane.showMessageDialog(component, ErrorMessage,
                        "Inane error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return valid;
    }
    
    /**
     * warning when a TextBox is empty or equals to 0
     * @param textbox
     * @param component
     * @param ErrorMessage
     * @return
     */
    public static ValidationState warning(TextBox textbox, Component component, String ErrorMessage){
        ValidationState valid = ValidationState.INVALID;
        try{
            if(!textbox.getText().isEmpty() && !textbox.getText().equals("0")){
                valid = ValidationState.VALID;
            }else{
                JOptionPane.showMessageDialog(component, ErrorMessage,
                        "Inane warning",
                        JOptionPane.WARNING_MESSAGE);
            }
        }catch(Exception ex){
            ErrorHandling.output(Level.SEVERE, ex);
        }
        return valid;
    }
}
