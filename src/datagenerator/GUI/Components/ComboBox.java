/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.GUI.Components;

import datagenerator.Utils.ErrorHandling;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.logging.Level;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mark
 */
public class ComboBox {
    private final String labelText;
    private final String toolTipText;
    private final GridBagConstraints c;
    private final int row;
    private final JPanel panel;
    private final Font font;
    private JComboBox comboBox;
    
    /**
     * Create a file chooser row
     * @param panel the panel where this file choose should be inserted
     * @param row the row index where this file choose should be placed
     * @param labelText the label of the label
     * @param toolTipText the tool tip text
     * @param font the font to be used
     */
    public ComboBox(JPanel panel, int row, String labelText, String toolTipText, Font font){
        this.row = row;
        this.panel = panel;
        this.labelText = labelText;
        this.toolTipText = toolTipText;
        this.font = font;
        
        //set the default directory
        this.c = new GridBagConstraints();
        this.comboBox = new JComboBox();
        
        generateRow();
    }
    
    public JComboBox getComboBox(){
        return this.comboBox;
    }
    
    private void generateRow(){
        try{
            //add the button and text field
            JLabel label = new JLabel(labelText);
            label.setFont(font);
            if(!toolTipText.isEmpty()){
                label.setToolTipText(toolTipText);
            }
            c.gridx = 0;
            c.gridy = row;
            c.weightx = 0.2;
            panel.add(label, c);
            
            this.comboBox.setPrototypeDisplayValue("XXXXXXXXXXXXXXXXX");
            this.comboBox.setBackground(Color.WHITE);
            if(!toolTipText.isEmpty()){
                comboBox.setToolTipText(toolTipText);
            }
            c.insets = new Insets(0, 0, 5, 0);
            c.gridx = 1;
            c.gridy = row;
            panel.add(this.comboBox, c);
            
            //listener which will change the mouse pointer
            comboBox.addMouseMotionListener(new MouseMotionListener(){
                @Override
                public void mouseMoved(MouseEvent  e) {
                    try{
                        comboBox.setCursor(new Cursor(Cursor.HAND_CURSOR));
                    }catch(Exception ex){
                        ErrorHandling.output(Level.WARNING, ex);
                    }
                }
                @Override
                public void mouseDragged(MouseEvent e) {
                }
            });
        }catch(Exception ex){
            ErrorHandling.output(Level.WARNING, ex);
        }
    }
    
    /**
     * Get a list of selected items
     * @return
     */
    public Object getSelected(){
        return this.comboBox.getSelectedItem();
    }
    
    /**
     * Update the list of items
     * @param data
     */
    public void updateList(Object[] data){
        ComboBoxModel model = new DefaultComboBoxModel(data);
        this.comboBox.setModel(model);
    }
    
    public void clearList() {
        this.comboBox.removeAllItems();
    }
}
