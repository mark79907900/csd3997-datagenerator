/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package datagenerator.GUI;

import datagenerator.GUI.Components.CheckBox;
import datagenerator.GUI.Components.ListBox;
import datagenerator.GUI.Components.Panel;
import datagenerator.GUI.Components.TextBox;
import datagenerator.Types.Bias;
import datagenerator.Types.BiasList;
import datagenerator.Types.GPSRoutes;
import datagenerator.Utils.Config;
import datagenerator.Utils.ErrorHandling;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.logging.Level;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Mark
 */
public class RemoveBiasGUI extends JFrame{
    private GPSRoutes routes;
    private BiasList biasList;
    private JPanel topPanel;
    private JPanel buttonsPanel;
    private boolean generatePassengerData;
    private Font font;
    private final RemoveBiasGUI self;
    private CheckBox gPSDataCheckBox;
    private CheckBox passengerDataCheckBox;
    private TextBox routeId;
    private TextBox stopId;
    private TextBox timeStamp;
    private TextBox minNumberOfGPSPoints;
    private TextBox maxNumberOfGPSPoints;
    private TextBox minNumberOfPassengerPoints;
    private TextBox maxNumberOfPassengerPoints;
    private ListBox baisId;
    
    public RemoveBiasGUI(BiasList biasList, GPSRoutes routes, boolean generatePassengerData, Font font) {
        super(Config.getPropertyString("removeBiasWindowName"));
        this.routes = routes;
        this.biasList = biasList;
        this.font = font;
        this.generatePassengerData = generatePassengerData;
        this.self = this;
        
        //set the defulat minimum window size
        setSize(
                Config.getPropertyInt("removeBiasWindowWidth"),
                Config.getPropertyInt("removeBiasWindowHeight")
        );
        
        //center of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
        setResizable(false);
        setLayout(new BorderLayout());
        
        //disable main window
        MainWindow.getMainWindow().setEnabled(false);
        
        //re-enable main window when this windows is closed
        addWindowListener(new WindowListener() {
            
            @Override
            public void windowOpened(WindowEvent e) {
            }
            
            @Override
            public void windowClosing(WindowEvent e) {
                MainWindow.getMainWindow().setEnabled(true);
            }
            
            @Override
            public void windowClosed(WindowEvent e) {
            }
            
            @Override
            public void windowIconified(WindowEvent e) {
            }
            
            @Override
            public void windowDeiconified(WindowEvent e) {
            }
            
            @Override
            public void windowActivated(WindowEvent e) {
            }
            
            @Override
            public void windowDeactivated(WindowEvent e) {
            }
        });
        
        setup();
    }
    
    /**
     * private method which loads the window gui
     */
    private void setup(){
        this.topPanel = new Panel("", "", Panel.panelType.GridBagLayout).getPanel();
        topPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        baisId = new ListBox(topPanel, 0,
                Config.getPropertyString("removeBiasBiasIDLabel"),
                Config.getPropertyString("removeBiasBiasIDToolTip"),
                font
        );
        baisId.updateList(biasList.getBiasIdObjectArray());
        baisId.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        gPSDataCheckBox = new CheckBox(topPanel, 1,
                Config.getPropertyString("addBiasGPSDataCheckBoxLabel"),
                Config.getPropertyString("addBiasGPSDataCheckBoxToolTip"),
                font
        );
        
        passengerDataCheckBox = new CheckBox(topPanel, 2,
                Config.getPropertyString("addBiasPassengerDataCheckBoxLabel"),
                Config.getPropertyString("addBiasPassengerDataCheckBoxToolTip"),
                font
        );
        passengerDataCheckBox.setEnabled(generatePassengerData);
        
        routeId = new TextBox(topPanel, 3,
                Config.getPropertyString("removeBiasRouteIdLabel"),
                Config.getPropertyString("removeBiasRouteIdToolTip"),
                font
        );
        routeId.setEnabled(false);
        
        stopId = new TextBox(topPanel, 4,
                Config.getPropertyString("removeBiasStopIdLabel"),
                Config.getPropertyString("removeBiasStopIdToolTip"),
                font
        );
        stopId.setEnabled(false);
        
        timeStamp = new TextBox(topPanel, 5,
                Config.getPropertyString("addBiasTimeStampLabel"),
                Config.getPropertyString("addBiasTimeStampToolTip"),
                font
        );
        
        minNumberOfGPSPoints = new TextBox(topPanel, 6,
                Config.getPropertyString("addBiasMinNumberOfGPSPointsLabel"),
                Config.getPropertyString("addBiasMinNumberOfGPSPointsToolTip"),
                font
        );
        minNumberOfGPSPoints.setEnabled(false);
        
        
        maxNumberOfGPSPoints = new TextBox(topPanel, 7,
                Config.getPropertyString("addBiasMaxNumberOfGPSPointsLabel"),
                Config.getPropertyString("addBiasMaxNumberOfGPSPointsToolTip"),
                font
        );
        maxNumberOfGPSPoints.setEnabled(false);
        
        minNumberOfPassengerPoints = new TextBox(topPanel, 8,
                Config.getPropertyString("addBiasMinNumberOfPassengerPointsLabel"),
                Config.getPropertyString("addBiasMinNumberOfPassengerPointsToolTip"),
                font
        );
        minNumberOfPassengerPoints.setEnabled(false);
        
        maxNumberOfPassengerPoints = new TextBox(topPanel, 9,
                Config.getPropertyString("addBiasMaxNumberOfPassengerPointsLabel"),
                Config.getPropertyString("addBiasMaxNumberOfPassengerPointsToolTip"),
                font
        );
        maxNumberOfPassengerPoints.setEnabled(false);
        
        //enable/disable fields related to the GPS data
        gPSDataCheckBox.getCheckBox().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED){
                    minNumberOfGPSPoints.setEnabled(true);
                    maxNumberOfGPSPoints.setEnabled(true);
                }else{
                    minNumberOfGPSPoints.setEnabled(false);
                    maxNumberOfGPSPoints.setEnabled(false);
                }
            }
        });
        
        //enable/disable fields related to the passenger data
        passengerDataCheckBox.getCheckBox().addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if(e.getStateChange() == ItemEvent.SELECTED){
                    minNumberOfPassengerPoints.setEnabled(true);
                    maxNumberOfPassengerPoints.setEnabled(true);
                }
                else{
                    minNumberOfPassengerPoints.setEnabled(false);
                    maxNumberOfPassengerPoints.setEnabled(false);
                }
            }
        });
        
        //actionListener which changes the values when selecting different items from the list
        baisId.getListBox().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                JList list = (JList)e.getSource();
                if(list.getSelectedValue() != null){
                    Bias bias = biasList.getBais((Long)list.getSelectedValue());
                    gPSDataCheckBox.setSelected(bias.isDumpGPSData());
                    passengerDataCheckBox.setEnabled(generatePassengerData);
                    passengerDataCheckBox.setSelected(bias.isDumpPassengerData());
                    
                    timeStamp.setValue(bias.getTimeStamp());
                    minNumberOfGPSPoints.setValue(bias.getMinGps());
                    maxNumberOfGPSPoints.setValue(bias.getMaxGps());
                    minNumberOfPassengerPoints.setValue(bias.getMinPassenger());
                    maxNumberOfPassengerPoints.setValue(bias.getMaxPassenger());
                    routeId.setValue(bias.getRouteId());
                    stopId.setValue(bias.getStopId());
                }
            }
        });
        
        this.add(this.topPanel, BorderLayout.NORTH);
        
        this.buttonsPanel = new Panel("", "", Panel.panelType.FlowLayout).getPanel();
        
        //close button
        JButton close = new JButton(Config.getPropertyString("removeBiasCancelLabel"));
        close.setToolTipText(Config.getPropertyString("removeBiasCancelToolTip"));
        close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    MainWindow.getMainWindow().setEnabled(true);
                    self.dispose();
                } catch (Exception ex) {
                    ErrorHandling.output(Level.SEVERE, ex);
                }
            }
        });
        this.buttonsPanel.add(close);
        
        //close button
        JButton remove = new JButton(Config.getPropertyString("removeBiasRemoveLabel"));
        remove.setToolTipText(Config.getPropertyString("removeBiasRemoveToolTip"));
        remove.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    long df = (long)baisId.getSelected().get(0);
                    biasList.remove((long)baisId.getSelected().get(0));
                    
                    resetFields();
                    
                } catch (Exception ex) {
                    ErrorHandling.output(Level.SEVERE, ex);
                }
            }
        });
        this.buttonsPanel.add(remove);
        
        //close button
        JButton update = new JButton(Config.getPropertyString("removeBiasUpdateLabel"));
        update.setToolTipText(Config.getPropertyString("removeBiasUpdateToolTip"));
        update.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if(gPSDataCheckBox.isSelected() || passengerDataCheckBox.isSelected()){
                        int minGPS = 0, maxGPS = 0, minPassenger = 0, maxPassenger = 0;
                        long time = 0;
                        boolean error = false;
                        
//                        if(!minNumberOfGPSPoints.getText().isEmpty()){
//                            minGPS = Integer.parseInt(minNumberOfGPSPoints.getText());
//                        }else{
//                            if(gPSDataCheckBox.isSelected()){
//                                JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiasMinNumberOfGPSPointsMessage"),
//                                        "Inane error",
//                                        JOptionPane.ERROR_MESSAGE);
//                                error = true;
//                            }
//                        }
//
//                        if(!maxNumberOfGPSPoints.getText().isEmpty()){
//                            maxGPS = Integer.parseInt(maxNumberOfGPSPoints.getText());
//                        }else{
//                            if(gPSDataCheckBox.isSelected()){
//                                JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiasMaxNumberOfGPSPointsMessage"),
//                                        "Inane error",
//                                        JOptionPane.ERROR_MESSAGE);
//                                error = true;
//                            }
//                        }
//
//
//                        if(!minNumberOfPassengerPoints.getText().isEmpty()){
//                            minPassenger = Integer.parseInt(minNumberOfPassengerPoints.getText());
//                        }else{
//                            if(passengerDataCheckBox.isSelected()){
//                                JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiasMinNumberOfPassengerPointsMessage"),
//                                        "Inane error",
//                                        JOptionPane.ERROR_MESSAGE);
//                                error = true;
//                            }
//                        }
//
//                        if(!maxNumberOfPassengerPoints.getText().isEmpty()){
//                            maxPassenger = Integer.parseInt(maxNumberOfPassengerPoints.getText());
//                        }else{
//                            if(passengerDataCheckBox.isSelected()){
//                                JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiasmaxNumberOfPassengerPointsMessage"),
//                                        "Inane error",
//                                        JOptionPane.ERROR_MESSAGE);
//                                error = true;
//                            }
//                        }
//
//                        if(!timeStamp.getText().isEmpty()){
//                            time = Long.parseLong(timeStamp.getText());
//                        }else{
//                            JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiastimeStampMessage"),
//                                    "Inane error",
//                                    JOptionPane.ERROR_MESSAGE);
//                            error = true;
//                        }
                        
                        Validations.ValidationState minGPSValidation = Validations.validation(
                                minNumberOfGPSPoints,
                                gPSDataCheckBox,
                                self,
                                Config.getPropertyString("addBiasMinNumberOfGPSPointsMessage")
                        );
                        
                        if(minGPSValidation == Validations.ValidationState.VALID){
                            minGPS = Integer.parseInt(minNumberOfGPSPoints.getText());
                        }else if (minGPSValidation == Validations.ValidationState.INVALID){
                            error = true;
                        }
                        
                        Validations.ValidationState maxGPSValidation = Validations.validation(
                                maxNumberOfGPSPoints,
                                gPSDataCheckBox,
                                self,
                                Config.getPropertyString("addBiasMaxNumberOfGPSPointsMessage")
                        );
                        
                        if(maxGPSValidation == Validations.ValidationState.VALID){
                            maxGPS = Integer.parseInt(maxNumberOfGPSPoints.getText());
                        }else if (maxGPSValidation == Validations.ValidationState.INVALID){
                            error = true;
                        }
                        
                        Validations.ValidationState minPassengerValidation = Validations.validation(
                                minNumberOfPassengerPoints,
                                passengerDataCheckBox,
                                self,
                                Config.getPropertyString("addBiasMinNumberOfPassengerPointsMessage")
                        );
                        
                        if(minPassengerValidation == Validations.ValidationState.VALID){
                            minPassenger = Integer.parseInt(minNumberOfPassengerPoints.getText());
                        }else if (minPassengerValidation == Validations.ValidationState.INVALID){
                            error = true;
                        }
                        
                        Validations.ValidationState maxPassengerValidation = Validations.validation(
                                maxNumberOfPassengerPoints,
                                passengerDataCheckBox,
                                self,
                                Config.getPropertyString("addBiasmaxNumberOfPassengerPointsMessage")
                        );
                        
                        if(maxPassengerValidation == Validations.ValidationState.VALID){
                            maxPassenger = Integer.parseInt(maxNumberOfPassengerPoints.getText());
                        }else if (maxPassengerValidation == Validations.ValidationState.INVALID){
                            error = true;
                        }
                        
                        Validations.ValidationState timeStampValidation = Validations.warning(
                                timeStamp,
                                self,
                                String.format(
                                        Config.getPropertyString("addBiastimeStampMessage"),
                                        Integer.parseInt(routeId.getText()),
                                        Integer.parseInt(stopId.getText())
                                )
                        );
                        
                        if(timeStampValidation == Validations.ValidationState.VALID){
                            time = Long.parseLong(timeStamp.getText());
                        }else if (timeStampValidation == Validations.ValidationState.INVALID){
                            time = 0;
                        }
                        
                        if(baisId.getSelected().size() < 1){
                            JOptionPane.showMessageDialog(self, Config.getPropertyString("removeBiasSelectbaisIdMessage"),
                                    "Inane error",
                                    JOptionPane.ERROR_MESSAGE);
                            error = true;
                        }
                        
                        if(!error && !routeId.getText().isEmpty()){
                            //save
                            Bias bias = new Bias(Integer.parseInt(routeId.getText()),
                                    Integer.parseInt(stopId.getText()),
                                    minGPS,
                                    maxGPS,
                                    minPassenger,
                                    maxPassenger,
                                    time,
                                    gPSDataCheckBox.isSelected(),
                                    passengerDataCheckBox.isSelected()
                            );
                            bias.setBiasId((long)baisId.getSelected().get(0));
                            biasList.update(bias);
                            
                            resetFields();
                        }
                        
                    }else{
                        JOptionPane.showMessageDialog(self, Config.getPropertyString("addBiasAtLeastOneSelectedMessage"),
                                "Inane error",
                                JOptionPane.ERROR_MESSAGE);
                    }
                } catch (Exception ex) {
                    ErrorHandling.output(Level.SEVERE, ex);
                }
            }
        });
        this.buttonsPanel.add(update);
        
        this.add(this.buttonsPanel, BorderLayout.SOUTH);
    }
    
    /***
     * Set the Frame to visible
     */
    public void setVisible(){
        setVisible(true);
    }
    
    /**
     * reset the fields of the form
     */
    private void resetFields(){
        baisId.updateList(biasList.getBiasIdObjectArray());
        minNumberOfGPSPoints.setEmpty();
        maxNumberOfGPSPoints.setEmpty();
        minNumberOfPassengerPoints.setEmpty();
        maxNumberOfPassengerPoints.setEmpty();
        timeStamp.setEmpty();
        routeId.setEmpty();
        stopId.setEmpty();
        gPSDataCheckBox.setSelected(false);
        passengerDataCheckBox.setSelected(false);
    }
}
